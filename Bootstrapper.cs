using Framework;
using Framework.AutoMapper;
using Framework.Clases.NHibernate;
using Microsoft.Extensions.DependencyInjection;

using System;
using JoiiBackend.AutoMapper;
using JoiiBackend.Clases.NHibernate;
using JoiiBackend.Controllers;
using JoiiBackend.Cruds;
using JoiiBackend.Cruds.Implementaciones;
using JoiiBackend.Services;
using JoiiBackend.Services.Implementaciones;

namespace JoiiBackend
{
    public class BootstrapperJoii : Bootstrapper<JoiiAppConfig, InscriptosController>
    {
        protected override void ConfigurarServiciosIniciales(IServiceCollection services)
        {
            base.ConfigurarServiciosIniciales(services);

            services.AddSingleton<IEntityMapper, JoiiAutoMapper>();
            services.AddSingleton<INHSessionFactory, JoiiNHSessionFactorySimple>();
        }

        protected override void ConfigurarServiciosPropios(IServiceCollection services)
        {
            base.ConfigurarServiciosPropios(services);

            services.AddScoped<ICrudVisita, CrudVisita>();
            services.AddScoped<ICrudUniversidad, CrudUniversidad>();
            services.AddScoped<ICrudSeminario, CrudSeminario>();
            services.AddScoped<ICrudCarrera, CrudCarrera>();
            services.AddScoped<ICrudInscripto, CrudInscripto>();
            services.AddScoped<ICrudInscriptoStateless, CrudInscriptoStateless>();
            services.AddScoped<ICrudConferencia, CrudConferencia>();
            services.AddScoped<ICrudMuestra, CrudMuestra>();
            services.AddScoped<ICrudConfiguracionGlobal, CrudConfiguracionGlobal>();
            services.AddScoped<ICrudAsistencia, CrudAsistencia>();
            services.AddScoped<ICrudActo, CrudActo>();

            services.AddScoped<IInscriptosService, InscriptosService>();
            services.AddScoped<ISeminariosService, SeminarioService>();
            services.AddScoped<IVisitaService, VisitaService>();
            services.AddScoped<ICarrerasService, CarrerasService>();
            services.AddScoped<IUniversidadesService, UniversidadesService>();
            services.AddScoped<IConferenciaService, ConferenciaService>();
            services.AddScoped<IMuestraService, MuestraService>();
            services.AddScoped<IConfiguracionGlobalService, ConfiguracionGlobalService>();
            services.AddScoped<IActoService, ActoService>();
        }

        protected override void ConfigurarServiciosEstaticosPropios(IServiceProvider provider)
        {
            base.ConfigurarServiciosEstaticosPropios(provider);
        }
    }
}
