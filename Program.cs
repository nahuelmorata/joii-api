using Framework;

using Microsoft.AspNetCore.Hosting;

namespace JoiiBackend
{
    public class JoiiProgram : FrameworkProgram
    {
        public static void Main(string[] args)
        {
            configurarWebHost = webBuilder =>
                webBuilder.UseStartup<Startup<BootstrapperJoii, JoiiAppConfig>>();
            
            Inicio(args);
        }
    }
}
