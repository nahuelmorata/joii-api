using JoiiBackend.Models;

namespace JoiiBackend.Extensiones
{
    public static class InscriptoExtension
    {
        public static Inscripto CargarEnNuevo(this Inscripto inscripto, Inscripto inscriptoDTO)
        {
            return new Inscripto
            {
                Id = inscripto.Id,
                Nombres = inscriptoDTO.Nombres,
                Apellidos = inscriptoDTO.Apellidos,
                Codigo = inscripto.Codigo,
                Dni = inscripto.Dni,
                Universidad = inscriptoDTO.Universidad,
                Lu = inscriptoDTO.Lu,
                Carrera = inscriptoDTO.Carrera,
                Email = inscriptoDTO.Email,
                Celular = inscriptoDTO.Celular,
                FechaNacimiento = inscriptoDTO.FechaNacimiento,
                Visita = inscripto.Visita,
                Seminario = inscripto.Seminario,
                SeminarioOpcional = inscripto.SeminarioOpcional,
                Conferencia = inscripto.Conferencia,
                Muestra = inscripto.Muestra,
                Fiesta = inscriptoDTO.Fiesta,
                Vegetariano = inscriptoDTO.Vegetariano,
                Vegano = inscriptoDTO.Vegano,
                Celiaco = inscriptoDTO.Celiaco,
                Certificado = inscriptoDTO.Certificado,
                FechaInscripcion = inscripto.FechaInscripcion,
                FechaUltimaModificacion = inscripto.FechaUltimaModificacion
            };
        }
    }
}