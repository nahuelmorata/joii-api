using AutoMapper;

using Framework.AutoMapper;
using FrameworkBase.Extension;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.AutoMapper
{
    public class JoiiAutoMapper : CoreEntityMapper
    {
        protected override void AgregarMapperPropio(IMapperConfigurationExpression config)
        {
            base.AgregarMapperPropio(config);

            config.CreateMap<Visita, VisitaDTO>()
                .ReverseMap();
            config.CreateMap<Universidad, UniversidadDTO>()
                .ReverseMap();
            config.CreateMap<Seminario, SeminarioDTO>()
                .ReverseMap();
            config.CreateMap<Carrera, CarreraDTO>()
                .ReverseMap();
            config.CreateMap<Inscripto, InscriptoDTO>()
                .ReverseMap()
                .ForMember(x => x.Universidad, conf => conf.EntityResolve(x => x.Universidad))
                .ForMember(x => x.Carrera, conf => conf.EntityResolve(x => x.Carrera))
                .ForMember(x => x.Visita, conf => conf.EntityResolve(x => x.Visita))
                .ForMember(x => x.Seminario, conf => conf.EntityResolve(x => x.Seminario))
                .ForMember(x => x.SeminarioOpcional, conf => conf.EntityResolve(x => x.SeminarioOpcional))
                .ForMember(x => x.Conferencia, conf => conf.EntityResolve(x => x.Conferencia))
                .ForMember(x => x.Muestra, conf => conf.EntityResolve(x => x.Muestra));
            config.CreateMap<Conferencia, ConferenciaDTO>()
                .ReverseMap();
            config.CreateMap<Muestra, MuestraDTO>()
                .ReverseMap();
            config.CreateMap<Acto, ActoDTO>()
                .ReverseMap();
        }
    }
}
