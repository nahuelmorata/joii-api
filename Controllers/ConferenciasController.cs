using Framework.Clases.Context;
using Framework.Controller;
using Framework.DTO;
using JoiiBackend.DTO;
using JoiiBackend.Services;
using Microsoft.AspNetCore.Mvc;

namespace JoiiBackend.Controllers
{
    [Route("/conferencias")]
    [ApiController]
    public class ConferenciasController : BaseControllerConAutenticacion
    {
        private readonly IConferenciaService _conferenciaService;

        public ConferenciasController(IContext context, IConferenciaService conferenciaService) : base(context)
        {
            _conferenciaService = conferenciaService;
        }

        [HttpGet]
        public ActionResult<ListaDTO<ConferenciaDTO>> ObtenerConferencias()
        {
            var conferencias = _conferenciaService.ObtenerTodas();
            return Ok(ConvertirALista(conferencias));
        }
        
        [HttpPost("anotar/{id:int}")]
        public ActionResult AnotarEnConferencia(int id)
        {
            _conferenciaService.Anotar(id);
            return Ok();
        }
        
        [HttpPost("marcarAsistencia")]
        public ActionResult<DTOOk> MarcarAsistenciaInscripto([FromBody] ControlInscriptoDTO controlInscriptoDTO)
        {
            return Ok(new DTOOk
            {
                Ok = _conferenciaService.MarcarAsistenciaInscripto(controlInscriptoDTO)
            });
        }
    }
}