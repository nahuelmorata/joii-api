using Framework.Clases.Context;
using Framework.Controller;
using JoiiBackend.DTO;
using JoiiBackend.Services;
using Microsoft.AspNetCore.Mvc;

namespace JoiiBackend.Controllers
{
    [ApiController]
    [Route("/configuracionGlobal")]
    public class ConfiguracionGlobalController : BaseControllerConAutenticacion
    {
        private readonly IConfiguracionGlobalService _configuracionGlobalService;

        public ConfiguracionGlobalController(IContext context, IConfiguracionGlobalService configuracionGlobalService)
            : base(context)
        {
            _configuracionGlobalService = configuracionGlobalService;
        }

        [HttpGet("fechaFin")]
        public ActionResult<FechaDTO> ObtenerFechaFin()
        {
            var fechaFin = _configuracionGlobalService.ObtenerFechaFin();
            return Ok(fechaFin);
        }
        
        [HttpGet("fechaDescarga")]
        public ActionResult<FechaDTO> ObtenerFechaDescarga()
        {
            var fechaFin = _configuracionGlobalService.ObtenerFechaDescarga();
            return Ok(fechaFin);
        }
    }
}