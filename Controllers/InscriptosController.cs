using System.Collections.Generic;
using Framework.Clases.Context;
using Framework.Controller;
using Framework.DTO;
using JoiiBackend.Clases.Enums;
using JoiiBackend.DTO;
using Microsoft.AspNetCore.Mvc;
using JoiiBackend.Services;
using Microsoft.AspNetCore.Authorization;

namespace JoiiBackend.Controllers
{
    [Route("/inscriptos")]
    [ApiController]
    public class InscriptosController : BaseControllerConAutenticacion
    {
        private readonly IInscriptosService _inscriptosService;

        public InscriptosController(IContext context, IInscriptosService inscriptosService) : base(context)
        {
            _inscriptosService = inscriptosService;
        }

        [HttpGet("contadores")]
        public ActionResult<ContadoresInscriptosDTO> ObtenerContadores()
        {
            var contadores = _inscriptosService.ObtenerContadores();
            return Ok(contadores);
        }

        [HttpGet]
        public ActionResult<ListaDTO<InscriptoDTO>> ObtenerInscriptos(
            [FromQuery] int? filtro, [FromQuery] TipoFiltroInscriptosEnum? tipoTipoFiltro)
        {
            var inscriptos = _inscriptosService.ObtenerTodos(filtro, tipoTipoFiltro);
            return Ok(ConvertirALista(inscriptos));
        }

        [HttpPost]
        public ActionResult AsociarInscripto([FromBody] InscriptoAsociarDTO inscriptoAsociarDTO)
        {
            _inscriptosService.Asociar(inscriptoAsociarDTO);
            return Ok();
        }

        [HttpPost("crear")]
        public ActionResult CrearInscriptos()
        {
            _inscriptosService.CrearUsuarios();
            return Ok();
        }

        [HttpGet("codigos")]
        public ActionResult<IList<int>> ObtenerCodigos()
        {
            var codigos = _inscriptosService.ObtenerCodigos();
            return Ok(codigos);
        }

        [HttpGet("inscripto")]
        public ActionResult<InscriptoDTO> ObtenerInscripto()
        {
            var inscripto = _inscriptosService.Obtener();
            return Ok(inscripto);
        }

        [HttpPut("inscripto")]
        public ActionResult<InscriptoDTO> ActualizarInscripto([FromBody] InscriptoDTO inscriptoDTO)
        {
            var inscripto = _inscriptosService.Actualizar(inscriptoDTO);
            return Ok(inscripto);
        }

        [HttpPost("control")]
        public ActionResult<DTOOk> ExisteInscripto([FromBody] ControlInscriptoDTO controlInscriptoDTO)
        {
            return Ok(new DTOOk
            {
                Ok = _inscriptosService.ExisteInscripto(controlInscriptoDTO)
            });
        }
        
        [HttpPost("control/fiesta")]
        public ActionResult<DTOOk> ExisteInscriptoFiesta([FromBody] ControlInscriptoDTO controlInscriptoDTO)
        {
            return Ok(new DTOOk
            {
                Ok = _inscriptosService.ExisteInscriptoFiesta(controlInscriptoDTO)
            });
        }

        [HttpPost("armarGrupos")]
        public ActionResult ArmarGrupo()
        {
            _inscriptosService.ArmarGrupos();
            return Ok();
        }
    }
}
