using Framework.Clases.Context;
using Framework.Controller;
using Framework.DTO;
using JoiiBackend.DTO;
using JoiiBackend.Services;
using Microsoft.AspNetCore.Mvc;

namespace JoiiBackend.Controllers
{
    [ApiController]
    [Route("/actos")]
    public class ActosController : BaseControllerConAutenticacion
    {
        private readonly IActoService _actoService;

        public ActosController(IContext context, IActoService actoService) : base(context)
        {
            _actoService = actoService;
        }
        
        [HttpGet]
        public ActionResult<ListaDTO<ActoDTO>> ObtenerActos()
        {
            var actos = _actoService.ObtenerActos();
            return Ok(ConvertirALista(actos));
        }
        
        [HttpPost("marcarAsistencia")]
        public ActionResult<DTOOk> MarcarAsistenciaInscripto([FromBody] ControlInscriptoDTO controlInscriptoDTO)
        {
            return Ok(new DTOOk
            {
                Ok = _actoService.MarcarAsistenciaInscripto(controlInscriptoDTO)
            });
        }
    }
}