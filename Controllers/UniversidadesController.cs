using Framework.Clases.Context;
using Framework.Controller;
using Framework.DTO;
using JoiiBackend.DTO;
using JoiiBackend.Services;
using Microsoft.AspNetCore.Mvc;

namespace JoiiBackend.Controllers
{
    [Route("/universidades")]
    [ApiController]
    public class UniversidadesController : BaseControllerConAutenticacion
    {
        private readonly IUniversidadesService _universidadesService;

        public UniversidadesController(IContext context, IUniversidadesService universidadesService) : base(context)
        {
            _universidadesService = universidadesService;
        }

        [HttpGet]
        public ActionResult<ListaDTO<UniversidadDTO>> ObtenerUniversidades()
        {
            var universidades = _universidadesService.ObtenerTodas();
            return Ok(ConvertirALista(universidades));
        }
    }
}