using Framework.Clases.Context;
using Framework.Controller;
using Framework.DTO;
using JoiiBackend.DTO;
using JoiiBackend.Services;
using Microsoft.AspNetCore.Mvc;

namespace JoiiBackend.Controllers
{
    [Route("/visitas")]
    [ApiController]
    public class VisitaController : BaseControllerConAutenticacion
    {
        private readonly IVisitaService _visitaService;

        public VisitaController(IContext context, IVisitaService visitaService) : base(context)
        {
            _visitaService = visitaService;
        }
        
        [HttpGet]
        public ActionResult<ListaDTO<VisitaDTO>> ObtenerVisitas()
        {
            var visitas = _visitaService.ObtenerTodos();
            return Ok(ConvertirALista(visitas));
        }

        [HttpPost]
        public ActionResult<VisitaDTO> AgregarVisita([FromBody] VisitaDTO visitaDTO)
        {
            var visita = _visitaService.Agregar(visitaDTO);
            return Ok(visita);
        }
        
        [HttpPut("{id:int}")]
        public ActionResult<VisitaDTO> ActualizarVisita(int id, [FromBody] VisitaDTO visitaDTO)
        {
            var visita = _visitaService.Actualizar(id, visitaDTO);
            return Ok(visita);
        }

        [HttpDelete("{id:int}")]
        public ActionResult BorrarVisita(int id)
        {
            _visitaService.Borrar(id);
            return Ok();
        }
        
        [HttpPost("anotar/{id:int}")]
        public ActionResult AnotarEnMuestra(int id)
        {
            _visitaService.Anotar(id);
            return Ok();
        }
        
        [HttpPost("marcarAsistencia")]
        public ActionResult<DTOOk> MarcarAsistenciaInscripto([FromBody] ControlInscriptoDTO controlInscriptoDTO)
        {
            return Ok(new DTOOk
            {
                Ok = _visitaService.MarcarAsistenciaInscripto(controlInscriptoDTO)
            });
        }
    }
}