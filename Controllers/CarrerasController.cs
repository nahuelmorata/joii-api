using Framework.Clases.Context;
using Framework.Controller;
using Framework.DTO;
using JoiiBackend.DTO;
using JoiiBackend.Services;
using Microsoft.AspNetCore.Mvc;

namespace JoiiBackend.Controllers
{
    [Route("/carreras")]
    [ApiController]
    public class CarrerasController : BaseControllerConAutenticacion
    {
        private readonly ICarrerasService _carrerasService;

        public CarrerasController(IContext context, ICarrerasService carrerasService) : base(context)
        {
            _carrerasService = carrerasService;
        }

        [HttpGet]
        public ActionResult<ListaDTO<CarreraDTO>> ObtenerCarreras()
        {
            var carreras = _carrerasService.ObtenerCarreras();
            return Ok(ConvertirALista(carreras));
        }
    }
}