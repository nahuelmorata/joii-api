using Framework.Clases.Context;
using Framework.Controller;
using Framework.DTO;
using JoiiBackend.DTO;
using JoiiBackend.Services;
using Microsoft.AspNetCore.Mvc;

namespace JoiiBackend.Controllers
{
    [Route("/muestras")]
    [ApiController]
    public class MuestrasController : BaseControllerConAutenticacion
    {
        private readonly IMuestraService _muestraService;

        public MuestrasController(IContext context, IMuestraService muestraService) : base(context)
        {
            _muestraService = muestraService;
        }
        
        [HttpGet]
        public ActionResult<ListaDTO<MuestraDTO>> ObtenerConferencias()
        {
            var muestras = _muestraService.ObtenerTodas();
            return Ok(ConvertirALista(muestras));
        }
        
        [HttpPost("anotar/{id:int}")]
        public ActionResult AnotarEnMuestra(int id)
        {
            _muestraService.Anotar(id);
            return Ok();
        }
        
        [HttpPost("marcarAsistencia")]
        public ActionResult<DTOOk> MarcarAsistenciaInscripto([FromBody] ControlInscriptoDTO controlInscriptoDTO)
        {
            return Ok(new DTOOk
            {
                Ok = _muestraService.MarcarAsistenciaInscripto(controlInscriptoDTO)
            });
        }
    }
}