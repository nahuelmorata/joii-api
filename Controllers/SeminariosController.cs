using Framework.Clases.Context;
using Framework.Controller;
using Framework.DTO;
using JoiiBackend.DTO;
using JoiiBackend.Services;
using Microsoft.AspNetCore.Mvc;

namespace JoiiBackend.Controllers
{
    [Route("/seminarios")]
    [ApiController]
    public class SeminariosController : BaseControllerConAutenticacion
    {
        private readonly ISeminariosService _seminariosService;

        public SeminariosController(IContext context, ISeminariosService seminariosService) : base(context)
        {
            _seminariosService = seminariosService;
        }

        [HttpGet]
        public ActionResult<ListaDTO<SeminarioDTO>> ObtenerSeminarios()
        {
            var seminarios = _seminariosService.ObtenerSeminarios();
            return Ok(seminarios);
        }

        [HttpPost]
        public ActionResult<SeminarioDTO> AgregarSeminario([FromBody] SeminarioDTO seminarioDTO)
        {
            var seminario = _seminariosService.AgregarSeminario(seminarioDTO);
            return Ok(seminario);
        }

        [HttpPut("{id:int}")]
        public ActionResult<SeminarioDTO> ActualizarSeminario(int id, [FromBody] SeminarioDTO seminarioDTO)
        {
            var seminario = _seminariosService.ActualizarSeminario(id, seminarioDTO);
            return Ok(seminario);
        }

        [HttpDelete("{id:int}")]
        public ActionResult BorrarSeminario(int id)
        {
            _seminariosService.BorrarSeminario(id);
            return Ok();
        }

        [HttpPost("anotar/{id:int}")]
        public ActionResult AnotarEnSeminario(int id, [FromQuery] bool obligatorio)
        {
            _seminariosService.Anotar(id, obligatorio);
            return Ok();
        }
        
        [HttpPost("marcarAsistencia")]
        public ActionResult<DTOOk> MarcarAsistenciaInscripto([FromBody] ControlInscriptoDTO controlInscriptoDTO)
        {
            return Ok(new DTOOk
            {
                Ok = _seminariosService.MarcarAsistenciaInscripto(controlInscriptoDTO)
            });
        }
    }
}