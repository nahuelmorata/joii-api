using System;
using Framework.Models;

namespace JoiiBackend.Models
{
    public class Acto : ModelBaseConId
    {
        public virtual string Nombre { get; set; } = "";
        public virtual string Lugar { get; set; } = "";
        public virtual DateTime FechaHora { get; set; }
    }
}