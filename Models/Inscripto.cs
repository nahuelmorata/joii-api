using System;
using Framework.Models;

namespace JoiiBackend.Models
{
    public class Inscripto : ModelBaseConId
    {
        public virtual string Nombres { get; set; } = "";
        public virtual string Apellidos { get; set; } = "";
        public virtual int Codigo { get; set; }
        public virtual int Dni { get; set; }
        public virtual Universidad? Universidad { get; set; }
        public virtual int Lu { get; set; }
        public virtual Carrera? Carrera { get; set; }
        public virtual string Email { get; set; } = "";
        public virtual string Celular { get; set; } = "";
        public virtual DateTime? FechaNacimiento { get; set; }
        public virtual Visita? Visita { get; set; }
        public virtual Seminario? Seminario { get; set; }
        public virtual Seminario? SeminarioOpcional { get; set; }
        public virtual Conferencia? Conferencia { get; set; }
        public virtual Muestra? Muestra { get; set; }
        public virtual bool Fiesta { get; set; }
        public virtual bool Vegetariano { get; set; }
        public virtual bool Vegano { get; set; }
        public virtual bool Celiaco { get; set; }
        public virtual bool Certificado { get; set; }
        public virtual int? Grupo { get; set; }
        public virtual DateTime? FechaInscripcion { get; set; }
        public virtual DateTime? FechaUltimaModificacion { get; set; }
    }
}