using System;
using Framework.Models;

namespace JoiiBackend.Models
{
    public class ConfiguracionGlobal : ModelBaseConId
    {
        public virtual DateTime FechaFin { get; set; }
        public virtual DateTime FechaDescarga { get; set; }
    }
}