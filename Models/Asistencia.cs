using Framework.Models;
using JoiiBackend.Clases.Enums;

namespace JoiiBackend.Models
{
    public class Asistencia : ModelBaseConId
    {
        public virtual Inscripto Inscripto { get; set; } = new();
        public virtual int Evento { get; set; }
        public virtual TipoEventoEnum Tipo { get; set; }
    }
}