using System;
using Framework.Models;

namespace JoiiBackend.Models
{
    public class Seminario : ModelBaseConId
    {
        public virtual string Nombre { get; set; } = "";
        public virtual string Lugar { get; set; } = "";
        public virtual DateTime FechaHora { get; set; } = DateTime.Now;
        public virtual int CupoObligatorio { get; set; }
        public virtual int CupoOpcional { get; set; }
        public virtual int OcupadoObligatorio { get; set; }
        public virtual int OcupadoOpcional { get; set; }
    }
}