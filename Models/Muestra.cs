using System;
using Framework.Models;

namespace JoiiBackend.Models
{
    public class Muestra : ModelBaseConId
    {
        public virtual DateTime FechaHora { get; set; } = DateTime.Now;
        public virtual int Cupo { get; set; } = 0;
        public virtual int Ocupado { get; set; } = 0;
    }
}