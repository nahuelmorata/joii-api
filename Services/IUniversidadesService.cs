using System.Collections.Generic;
using Framework.Services;
using JoiiBackend.DTO;

namespace JoiiBackend.Services
{
    public interface IUniversidadesService : IService
    {
        IList<UniversidadDTO> ObtenerTodas();
    }
}