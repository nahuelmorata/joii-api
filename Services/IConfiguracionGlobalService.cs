using System;
using Framework.Services;
using JoiiBackend.DTO;

namespace JoiiBackend.Services
{
    public interface IConfiguracionGlobalService : IService
    {
        FechaDTO ObtenerFechaFin();
        FechaDTO ObtenerFechaDescarga();
    }
}