using System.Collections.Generic;
using Framework.Services;
using JoiiBackend.DTO;

namespace JoiiBackend.Services
{
    public interface ICarrerasService : IService
    {
        IList<CarreraDTO> ObtenerCarreras();
    }
}