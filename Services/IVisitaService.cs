using System.Collections.Generic;
using Framework.Services;
using JoiiBackend.DTO;

namespace JoiiBackend.Services
{
    public interface IVisitaService : IService
    {
        VisitaDTO Agregar(VisitaDTO visitaDTO);
        IList<VisitaDTO> ObtenerTodos();
        VisitaDTO Actualizar(int id, VisitaDTO visitaDTO);
        void Borrar(int id);
        void Anotar(int id);
        bool MarcarAsistenciaInscripto(ControlInscriptoDTO controlInscriptoDTO);
    }
}