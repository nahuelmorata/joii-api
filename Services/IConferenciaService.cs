using System.Collections.Generic;
using Framework.Services;
using JoiiBackend.DTO;

namespace JoiiBackend.Services
{
    public interface IConferenciaService : IService
    {
        IList<ConferenciaDTO> ObtenerTodas();
        void Anotar(int id);
        bool MarcarAsistenciaInscripto(ControlInscriptoDTO controlInscriptoDTO);
    }
}