using System.Collections.Generic;
using Framework.Services;
using JoiiBackend.DTO;

namespace JoiiBackend.Services
{
    public interface IActoService : IService
    {
        bool MarcarAsistenciaInscripto(ControlInscriptoDTO controlInscriptoDTO);
        IList<ActoDTO> ObtenerActos();
    }
}