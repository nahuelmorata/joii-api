using System.Collections.Generic;
using Framework.Services;
using JoiiBackend.DTO;

namespace JoiiBackend.Services
{
    public interface IMuestraService : IService
    {
        IList<MuestraDTO> ObtenerTodas();
        void Anotar(int id);
        bool MarcarAsistenciaInscripto(ControlInscriptoDTO controlInscriptoDTO);
    }
}