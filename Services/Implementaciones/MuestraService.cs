using System;
using System.Collections.Generic;
using AutoMapper;
using Framework.Clases.Excepciones;
using Framework.Services;
using JoiiBackend.Clases.Enums;
using JoiiBackend.Cruds;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Services.Implementaciones
{
    public class MuestraService : BaseService, IMuestraService
    {
        private readonly ICrudMuestra _crudMuestra;
        private readonly ICrudInscripto _crudInscripto;
        private readonly ICrudAsistencia _crudAsistencia;

        public MuestraService(IMapper mapper, ICrudMuestra crudMuestra, ICrudInscripto crudInscripto,
            ICrudAsistencia crudAsistencia) : base(mapper)
        {
            _crudMuestra = crudMuestra;
            _crudInscripto = crudInscripto;
            _crudAsistencia = crudAsistencia;
        }
        
        public IList<MuestraDTO> ObtenerTodas()
        {
            var muestras = _crudMuestra.ObtenerTodos();
            return Mapper.ConvertirListaAListaDTO<Muestra, MuestraDTO>(muestras);
        }
        
        public void Anotar(int id)
        {
            var muestra = _crudMuestra.ObtenerPorId(id);
            if (muestra == null)
                throw new ExcepcionControlada("La muestra no existe.");
            var inscripto = _crudInscripto.ObtenerPorDni(int.Parse(Usuario!.User));
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            if (inscripto.Muestra?.Id == id)
                return;
            if (muestra.Cupo <= muestra.Ocupado)
                throw new ExcepcionControlada($"El cupo de la muestra en el horario {muestra.FechaHora:HH:mm} esta lleno.");
            if (inscripto.Muestra != null)
            {
                inscripto.Muestra.Ocupado--;
                _crudMuestra.Actualizar(inscripto.Muestra);
            }

            muestra.Ocupado++;
            inscripto.Muestra = muestra;

            _crudMuestra.Actualizar(muestra);
            inscripto.FechaUltimaModificacion = DateTime.Now;
            _crudInscripto.Actualizar(inscripto);
        }
        
        public bool MarcarAsistenciaInscripto(ControlInscriptoDTO controlInscriptoDTO)
        {
            if (_crudAsistencia.ExisteAsistencia(controlInscriptoDTO, TipoEventoEnum.Muestra))
                return true;
            var inscripto = _crudInscripto.ObtenerPorDni(controlInscriptoDTO.Dni);
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            if (controlInscriptoDTO.IdEvento == null)
                throw new ExcepcionControlada("Debe enviar una muestra para controlar.");
            if (inscripto.Muestra == null)
                return false;
            
            var estaInscripto = inscripto.Muestra.Id == controlInscriptoDTO.IdEvento;
            if (estaInscripto)
                _crudAsistencia.Insertar(new Asistencia
                {
                    Inscripto = inscripto,
                    Evento = (int) controlInscriptoDTO.IdEvento,
                    Tipo = TipoEventoEnum.Muestra
                });
            return estaInscripto;
        }
    }
}