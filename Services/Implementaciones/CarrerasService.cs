using System.Collections.Generic;
using AutoMapper;
using Framework.Services;
using JoiiBackend.Cruds;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Services.Implementaciones
{
    public class CarrerasService : BaseService, ICarrerasService
    {
        private readonly ICrudCarrera _crudCarrera;

        public CarrerasService(IMapper mapper, ICrudCarrera crudCarrera) : base(mapper)
        {
            _crudCarrera = crudCarrera;
        }

        public IList<CarreraDTO> ObtenerCarreras()
        {
            var carreras = _crudCarrera.ObtenerTodos();
            return Mapper.ConvertirListaAListaDTO<Carrera, CarreraDTO>(carreras);
        }
    }
}