using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AutoMapper;
using Framework.Clases;
using Framework.Clases.Auth;
using Framework.Clases.Excepciones;
using Framework.Cruds;
using Framework.Models;
using Framework.Services;
using Framework.Utils;
using JoiiBackend.Clases;
using JoiiBackend.Clases.Enums;
using JoiiBackend.Cruds;
using JoiiBackend.DTO;
using JoiiBackend.Extensiones;
using JoiiBackend.Models;

namespace JoiiBackend.Services.Implementaciones
{
    public class InscriptosService : BaseService, IInscriptosService
    {
        private readonly ICrudInscriptoStateless _crudInscriptoStateless;
        private readonly ICrudInscripto _crudInscripto;
        private readonly ICrudUsuario _crudUsuario;
        private readonly IJwtAuthenticationService _jwtAuthenticationService;
        private readonly ICrudCarrera _crudCarrera;

        public InscriptosService(IMapper mapper, ICrudInscriptoStateless crudInscriptoStateless,
            ICrudInscripto crudInscripto, ICrudUsuario crudUsuario, IJwtAuthenticationService jwtAuthenticationService,
            ICrudCarrera crudCarrera) : base(mapper)
        {
            _crudInscriptoStateless = crudInscriptoStateless;
            _crudInscripto = crudInscripto;
            _crudUsuario = crudUsuario;
            _jwtAuthenticationService = jwtAuthenticationService;
            _crudCarrera = crudCarrera;
        }

        public void CrearUsuarios()
        {
            _crudInscriptoStateless.CrearInscriptos(500);
        }

        public IList<int> ObtenerCodigos()
        {
            var inscriptos = _crudInscriptoStateless.ObtenerTodos();
            return inscriptos.Where(x => x.Dni == 0)
                .Select(x => x.Codigo).ToList();
        }

        public void Asociar(InscriptoAsociarDTO inscriptoAsociarDTO)
        {
            if (_crudInscripto.EstaDniAsociado(inscriptoAsociarDTO.Dni))
                throw new ExcepcionControlada("El DNI ya esta asociado.");
            var inscripto = _crudInscripto.ObtenerPorCodigo(inscriptoAsociarDTO.Codigo);
            if (inscripto == null)
                throw new ExcepcionControlada("El codigo no existe.");
            inscripto.Dni = inscriptoAsociarDTO.Dni;
            inscripto.FechaInscripcion = DateTime.Now;
            _crudInscripto.Actualizar(inscripto);
            var usuario = new Usuario
            {
                Id = 0,
                User = inscriptoAsociarDTO.Dni.ToString(),
                Password = inscriptoAsociarDTO.Codigo.ToString().Encriptar(),
                Rol = new Rol
                {
                    Id = JoiiRolesEnum.Inscripto.Id
                }
            };
            _crudUsuario.Insertar(usuario);
        }

        public ContadoresInscriptosDTO ObtenerContadores()
        {
            var contadores = new ContadoresInscriptosDTO();
            var inscriptos = _crudInscripto.ObtenerTodosIniciaron();

            var horario1Inicio = DateTime.Today.AddHours(10);
            var horario1Fin = DateTime.Today.AddHours(14);
            var horario2Inicio = DateTime.Today.AddHours(16);
            var horario2Fin = DateTime.Today.AddHours(20);
            var ahora = DateTime.Now;
            var horarioFinalInicio = DateTime.Today;
            var horarioFinalFin = DateTime.Today;

            if (ahora > horario1Inicio && ahora < horario1Fin)
            {
                horarioFinalInicio = horario1Inicio;
                horarioFinalFin = horario1Fin;
            }

            if (ahora > horario2Inicio && ahora < horario2Fin) {
                horarioFinalInicio = horario2Inicio;
                horarioFinalFin = horario2Fin;
            }

            contadores.Global = inscriptos.Count;
            foreach (var inscripto in inscriptos)
                contadores.Dia += inscripto.FechaInscripcion > horarioFinalInicio &&
                                  inscripto.FechaInscripcion < horarioFinalFin ? 1 : 0;
            return contadores;
        }

        public IList<InscriptoDTO> ObtenerTodos(int? filtro, TipoFiltroInscriptosEnum? tipoFiltro)
        {
            IList<Inscripto> inscriptos = new List<Inscripto>();
            if (filtro == null || tipoFiltro == null)
            {
                inscriptos = _crudInscripto.ObtenerTodosIniciaron();
                return Mapper.ConvertirListaAListaDTO<Inscripto, InscriptoDTO>(inscriptos);
            }

            switch (tipoFiltro)
            {
                case TipoFiltroInscriptosEnum.Seminario:
                    inscriptos = _crudInscripto.ObtenerPorSeminario((int) filtro);
                    break;
                case TipoFiltroInscriptosEnum.Visita:
                    inscriptos = _crudInscripto.ObtenerPorVisita((int) filtro);
                    break;
                case TipoFiltroInscriptosEnum.Universidad:
                    inscriptos = _crudInscripto.ObtenerPorUniversidad((int) filtro);
                    break;
                case TipoFiltroInscriptosEnum.FechaInscripcion:
                    inscriptos = _crudInscripto.ObtenerPorFechaInscripcion((int) filtro);
                    break;
                default:
                    inscriptos = _crudInscripto.ObtenerTodosIniciaron();
                    break;
            }
            return Mapper.ConvertirListaAListaDTO<Inscripto, InscriptoDTO>(inscriptos);
        }

        public InscriptoDTO Obtener()
        {
            var inscripto = _crudInscripto.ObtenerPorDni(int.Parse(Usuario!.User));
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            return Mapper.ConvertirADTO<Inscripto, InscriptoDTO>(inscripto);
        }

        public InscriptoDTO Actualizar(InscriptoDTO inscriptoDTO)
        {
            var inscripto = _crudInscripto.ObtenerPorDni(int.Parse(Usuario!.User));
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            var inscriptoModificacion = Mapper.ConvertirAEntidad<InscriptoDTO, Inscripto>(inscriptoDTO);
            inscripto = inscripto.CargarEnNuevo(inscriptoModificacion);
            inscripto.FechaUltimaModificacion = DateTime.Now;
            inscripto = _crudInscripto.Actualizar(inscripto);
            return Mapper.ConvertirADTO<Inscripto, InscriptoDTO>(inscripto);
        }

        public bool ExisteInscripto(ControlInscriptoDTO controlInscriptoDTO)
        {
            var inscripto = _crudInscripto.ObtenerPorDni(controlInscriptoDTO.Dni);
            return inscripto?.FechaUltimaModificacion != null;
        }

        public bool ExisteInscriptoFiesta(ControlInscriptoDTO controlInscriptoDTO)
        {
            var inscripto = _crudInscripto.ObtenerPorDni(controlInscriptoDTO.Dni);
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            return inscripto.Fiesta;
        }

        public void ArmarGrupos()
        {
            var inscriptos = _crudInscripto.ObtenerTodosIniciaron();
            var carreras = _crudCarrera.ObtenerTodos();
            var dniIgnorar = new List<int> { 41198668, 40981580, 41289481, 39869975, 39870360, 42091159, 42541011,
                42195381, 41145590, 41315496, 41568925, 38803857, 40210547, 40374563, 41014536, 39869942, 39877771,
                41071782
            };
            var cursorGrupo = 1;

            foreach (var carrera in carreras)
            {
                var inscriptosCarrera = inscriptos.Where(x => x.Carrera?.Id == carrera.Id);
                foreach (var inscripto in inscriptosCarrera)
                {
                    if (dniIgnorar.Contains(inscripto.Dni))
                        inscripto.Grupo = null;
                    else
                    {
                        if (cursorGrupo == 15)
                            cursorGrupo = 1;
                        inscripto.Grupo = cursorGrupo;
                        cursorGrupo++;
                    }

                    _crudInscripto.Actualizar(inscripto);
                }
            }
        }
    }
}