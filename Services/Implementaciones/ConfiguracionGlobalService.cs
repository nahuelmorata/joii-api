using System;
using AutoMapper;
using Framework.Services;
using JoiiBackend.Cruds;
using JoiiBackend.DTO;

namespace JoiiBackend.Services.Implementaciones
{
    public class ConfiguracionGlobalService : BaseService, IConfiguracionGlobalService
    {
        private readonly ICrudConfiguracionGlobal _crudConfiguracionGlobal;

        public ConfiguracionGlobalService(IMapper mapper, ICrudConfiguracionGlobal crudConfiguracionGlobal)
            : base(mapper)
        {
            _crudConfiguracionGlobal = crudConfiguracionGlobal;
        }

        public FechaDTO ObtenerFechaFin()
        {
            var configuracion = _crudConfiguracionGlobal.ObtenerConfiguracion();
            return new FechaDTO { Fecha = configuracion.FechaFin };
        }

        public FechaDTO ObtenerFechaDescarga()
        {
            var configuracion = _crudConfiguracionGlobal.ObtenerConfiguracion();
            return new FechaDTO { Fecha = configuracion.FechaDescarga };
        }
    }
}