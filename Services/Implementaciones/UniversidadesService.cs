using System.Collections.Generic;
using AutoMapper;
using Framework.Services;
using JoiiBackend.Cruds;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Services.Implementaciones
{
    public class UniversidadesService : BaseService, IUniversidadesService
    {
        private readonly ICrudUniversidad _crudUniversidad;

        public UniversidadesService(IMapper mapper, ICrudUniversidad crudUniversidad) : base(mapper)
        {
            _crudUniversidad = crudUniversidad;
        }

        public IList<UniversidadDTO> ObtenerTodas()
        {
            var universidades = _crudUniversidad.ObtenerTodos();
            return Mapper.ConvertirListaAListaDTO<Universidad, UniversidadDTO>(universidades);
        }
    }
}