using System.Collections.Generic;
using AutoMapper;
using Framework.Clases.Excepciones;
using Framework.Services;
using JoiiBackend.Clases.Enums;
using JoiiBackend.Cruds;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Services.Implementaciones
{
    public class ActoService : BaseService, IActoService
    {
        private readonly ICrudActo _crudActo;
        private readonly ICrudInscripto _crudInscripto;
        private readonly ICrudAsistencia _crudAsistencia;

        public ActoService(IMapper mapper, ICrudActo crudActo, ICrudInscripto crudInscripto,
            ICrudAsistencia crudAsistencia) : base(mapper)
        {
            _crudActo = crudActo;
            _crudInscripto = crudInscripto;
            _crudAsistencia = crudAsistencia;
        }

        public bool MarcarAsistenciaInscripto(ControlInscriptoDTO controlInscriptoDTO)
        {
            var inscripto = _crudInscripto.ObtenerPorDni(controlInscriptoDTO.Dni);
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            if (controlInscriptoDTO.IdEvento == null)
                throw new ExcepcionControlada("Debe enviar un acto para controlar.");
            if (inscripto.FechaUltimaModificacion == null)
                throw new ExcepcionControlada("No accedio al sistema para cargar sus datos.");
            if (_crudActo.ObtenerPorId((int) controlInscriptoDTO.IdEvento) == null)
                throw new ExcepcionControlada("No existe el acto.");
            if (_crudAsistencia.ExisteAsistencia(controlInscriptoDTO, TipoEventoEnum.Acto))
                return true;
            _crudAsistencia.Insertar(new Asistencia
            {
                Inscripto = inscripto,
                Evento = (int) controlInscriptoDTO.IdEvento,
                Tipo = TipoEventoEnum.Acto
            });
                
            return true;
        }

        public IList<ActoDTO> ObtenerActos()
        {
            var actos = _crudActo.ObtenerTodos();
            return Mapper.ConvertirListaAListaDTO<Acto, ActoDTO>(actos);
        }
    }
}