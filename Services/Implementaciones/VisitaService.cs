using System;
using System.Collections.Generic;
using AutoMapper;
using Framework.Clases.Excepciones;
using Framework.Services;
using JoiiBackend.Clases.Enums;
using JoiiBackend.Cruds;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Services.Implementaciones
{
    public class VisitaService : BaseService, IVisitaService
    {
        private readonly ICrudVisita _crudVisita;
        private readonly ICrudInscripto _crudInscripto;
        private readonly ICrudAsistencia _crudAsistencia;

        public VisitaService(IMapper mapper, ICrudVisita crudVisita, ICrudInscripto crudInscripto,
            ICrudAsistencia crudAsistencia) : base(mapper)
        {
            _crudVisita = crudVisita;
            _crudInscripto = crudInscripto;
            _crudAsistencia = crudAsistencia;
        }

        public VisitaDTO Agregar(VisitaDTO visitaDTO)
        {
            if (_crudVisita.ExisteVisita(visitaDTO))
                throw new ExcepcionControlada("La visita ya existe");
            var visita = Mapper.ConvertirAEntidad<VisitaDTO, Visita>(visitaDTO);
            visita.Ocupado = 0;
            visita = _crudVisita.Insertar(visita);
            return Mapper.ConvertirADTO<Visita, VisitaDTO>(visita);
        }

        public IList<VisitaDTO> ObtenerTodos()
        {
            var visitas = _crudVisita.ObtenerTodos();
            return Mapper.ConvertirListaAListaDTO<Visita, VisitaDTO>(visitas);
        }

        public VisitaDTO Actualizar(int id, VisitaDTO visitaDTO)
        {
            if (id != visitaDTO.Id)
                throw new ExcepcionControlada("No puede modificar entidad, intente de nuevo");
            var entidad = _crudVisita.ObtenerPorId(id);
            if (entidad == null)
                throw new ExcepcionControlada("La visita no existe.");
            visitaDTO.Ocupado = entidad.Ocupado;
            entidad = Mapper.ConvertirAEntidadEn(visitaDTO, entidad);
            entidad.Id = id;
            entidad = _crudVisita.Actualizar(entidad);
            return Mapper.ConvertirADTO<Visita, VisitaDTO>(entidad);
        }

        public void Borrar(int id)
        {
            var entidad = _crudVisita.ObtenerPorId(id);
            if (entidad == null)
                throw new ExcepcionControlada("La visita no existe.");
            _crudVisita.Borrar(entidad);
        }
        
        public void Anotar(int id)
        {
            var visita = _crudVisita.ObtenerPorId(id);
            if (visita == null)
                throw new ExcepcionControlada("La visita no existe.");
            var inscripto = _crudInscripto.ObtenerPorDni(int.Parse(Usuario!.User));
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            if (inscripto.Visita?.Id == id)
                return;
            if (visita.Cupo <= visita.Ocupado)
                throw new ExcepcionControlada($"El cupo de la vista {visita.Nombre} esta lleno.");
            if (inscripto.Visita != null)
            {
                inscripto.Visita.Ocupado--;
                _crudVisita.Actualizar(inscripto.Visita);
            }

            visita.Ocupado++;
            inscripto.Visita = visita;

            _crudVisita.Actualizar(visita);
            inscripto.FechaUltimaModificacion = DateTime.Now;
            _crudInscripto.Actualizar(inscripto);
        }
        
        public bool MarcarAsistenciaInscripto(ControlInscriptoDTO controlInscriptoDTO)
        {
            if (_crudAsistencia.ExisteAsistencia(controlInscriptoDTO, TipoEventoEnum.Visita))
                return true;
            var inscripto = _crudInscripto.ObtenerPorDni(controlInscriptoDTO.Dni);
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            if (controlInscriptoDTO.IdEvento == null)
                throw new ExcepcionControlada("Debe enviar una visita para controlar.");
            if (inscripto.Visita == null)
                return false;
            
            var estaInscripto = inscripto.Visita.Id == controlInscriptoDTO.IdEvento;
            if (estaInscripto)
                _crudAsistencia.Insertar(new Asistencia
                {
                    Inscripto = inscripto,
                    Evento = (int) controlInscriptoDTO.IdEvento,
                    Tipo = TipoEventoEnum.Visita
                });
            return estaInscripto;
        }
    }
}