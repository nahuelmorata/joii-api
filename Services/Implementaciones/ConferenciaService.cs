using System;
using System.Collections.Generic;
using AutoMapper;
using Framework.Clases.Excepciones;
using Framework.Services;
using JoiiBackend.Clases.Enums;
using JoiiBackend.Cruds;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Services.Implementaciones
{
    public class ConferenciaService : BaseService, IConferenciaService
    {
        private readonly ICrudConferencia _crudConferencia;
        private readonly ICrudAsistencia _crudAsistencia;
        private readonly ICrudInscripto _crudInscripto;

        public ConferenciaService(IMapper mapper, ICrudConferencia crudConferencia, ICrudAsistencia crudAsistencia,
            ICrudInscripto crudInscripto) : base(mapper)
        {
            _crudConferencia = crudConferencia;
            _crudAsistencia = crudAsistencia;
            _crudInscripto = crudInscripto;
        }

        public IList<ConferenciaDTO> ObtenerTodas()
        {
            var conferencias = _crudConferencia.ObtenerTodos();
            return Mapper.ConvertirListaAListaDTO<Conferencia, ConferenciaDTO>(conferencias);
        }

        public void Anotar(int id)
        {
            var conferencia = _crudConferencia.ObtenerPorId(id);
            if (conferencia == null)
                throw new ExcepcionControlada("La conferencia no existe.");
            var inscripto = _crudInscripto.ObtenerPorDni(int.Parse(Usuario!.User));
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            if (inscripto.Conferencia?.Id == id)
                return;
            if (conferencia.Cupo <= conferencia.Ocupado)
                throw new ExcepcionControlada($"El cupo de la conferencia {conferencia.Nombre} esta lleno.");
            if (inscripto.Conferencia != null)
            {
                inscripto.Conferencia.Ocupado--;
                _crudConferencia.Actualizar(inscripto.Conferencia);
            }

            conferencia.Ocupado++;
            inscripto.Conferencia = conferencia;

            _crudConferencia.Actualizar(conferencia);
            inscripto.FechaUltimaModificacion = DateTime.Now;
            _crudInscripto.Actualizar(inscripto);
        }

        public bool MarcarAsistenciaInscripto(ControlInscriptoDTO controlInscriptoDTO)
        {
            if (_crudAsistencia.ExisteAsistencia(controlInscriptoDTO, TipoEventoEnum.Conferencia))
                return true;
            var inscripto = _crudInscripto.ObtenerPorDni(controlInscriptoDTO.Dni);
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            if (controlInscriptoDTO.IdEvento == null)
                throw new ExcepcionControlada("Debe enviar una conferencia para controlar.");
            if (inscripto.Conferencia == null)
                return false;
            
            var estaInscripto = inscripto.Conferencia.Id == controlInscriptoDTO.IdEvento;
            if (estaInscripto)
                _crudAsistencia.Insertar(new Asistencia
                {
                    Inscripto = inscripto,
                    Evento = (int) controlInscriptoDTO.IdEvento,
                    Tipo = TipoEventoEnum.Conferencia
                });
            return estaInscripto;
        }
    }
}