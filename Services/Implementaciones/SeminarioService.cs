using System;
using System.Collections.Generic;
using AutoMapper;
using Framework.Clases.Excepciones;
using Framework.Services;
using JoiiBackend.Clases.Enums;
using JoiiBackend.Cruds;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Services.Implementaciones
{
    public class SeminarioService : BaseService, ISeminariosService
    {
        private readonly ICrudSeminario _crudSeminario;
        private readonly ICrudInscripto _crudInscripto;
        private readonly ICrudAsistencia _crudAsistencia;

        public SeminarioService(IMapper mapper, ICrudSeminario crudSeminario, ICrudInscripto crudInscripto,
            ICrudAsistencia crudAsistencia) : base(mapper)
        {
            _crudSeminario = crudSeminario;
            _crudInscripto = crudInscripto;
            _crudAsistencia = crudAsistencia;
        }

        public SeminarioDTO AgregarSeminario(SeminarioDTO seminarioDTO)
        {
            if (_crudSeminario.ExisteSeminario(seminarioDTO))
                throw new ExcepcionControlada("El seminario ya existe");
            var seminario = Mapper.ConvertirAEntidad<SeminarioDTO, Seminario>(seminarioDTO);
            seminario.OcupadoObligatorio = 0;
            seminario.OcupadoOpcional = 0;
            seminario = _crudSeminario.Insertar(seminario);
            return Mapper.ConvertirADTO<Seminario, SeminarioDTO>(seminario);
        }

        public IList<SeminarioDTO> ObtenerSeminarios()
        {
            var seminarios = _crudSeminario.ObtenerTodos();
            return Mapper.ConvertirListaAListaDTO<Seminario, SeminarioDTO>(seminarios);
        }

        public SeminarioDTO ActualizarSeminario(int id, SeminarioDTO seminarioDTO)
        {
            if (id != seminarioDTO.Id)
                throw new ExcepcionControlada("No puede modificar entidad, intente de nuevo");
            var entidad = _crudSeminario.ObtenerPorId(id);
            if (entidad == null)
                throw new ExcepcionControlada("El seminario no existe.");
            if (seminarioDTO.CupoObligatorio < entidad.OcupadoObligatorio)
                throw new ExcepcionControlada("El cupo obligatorio no puede ser menor a la cantidad ocupada.");
            if (seminarioDTO.CupoOpcional < entidad.OcupadoOpcional)
                throw new ExcepcionControlada("El cupo opcional no puede ser menor a la cantidad ocupada.");
            seminarioDTO.OcupadoObligatorio = entidad.OcupadoObligatorio;
            seminarioDTO.OcupadoOpcional = entidad.OcupadoOpcional;
            entidad = Mapper.ConvertirAEntidadEn(seminarioDTO, entidad);
            entidad.Id = id;
            entidad = _crudSeminario.Actualizar(entidad);
            return Mapper.ConvertirADTO<Seminario, SeminarioDTO>(entidad);
        }

        public void BorrarSeminario(int id)
        {
            var entidad = _crudSeminario.ObtenerPorId(id);
            if (entidad == null)
                throw new ExcepcionControlada("El seminario no existe.");
            _crudSeminario.Borrar(entidad);
        }

        public void Anotar(int id, bool obligatorio)
        {
            var seminario = _crudSeminario.ObtenerPorId(id);
            if (seminario == null)
                throw new ExcepcionControlada("El seminario no existe.");
            var inscripto = _crudInscripto.ObtenerPorDni(int.Parse(Usuario!.User));
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            if (obligatorio)
            {
                if (inscripto.Seminario?.Id == id)
                    return;
                if (seminario.CupoObligatorio <= seminario.OcupadoObligatorio)
                    throw new ExcepcionControlada($"El cupo del seminario {seminario.Nombre} esta lleno.");
                if (inscripto.Seminario != null)
                {
                    inscripto.Seminario.OcupadoObligatorio--;
                    _crudSeminario.Actualizar(inscripto.Seminario);
                }

                seminario.OcupadoObligatorio++;
                inscripto.Seminario = seminario;
            }
            else
            {
                if (inscripto.SeminarioOpcional?.Id == id)
                    return;
                if (seminario.CupoOpcional <= seminario.OcupadoOpcional)
                    throw new ExcepcionControlada($"El cupo del seminario {seminario.Nombre} esta lleno.");
                if (inscripto.SeminarioOpcional != null)
                {
                    inscripto.SeminarioOpcional.OcupadoOpcional--;
                    _crudSeminario.Actualizar(inscripto.SeminarioOpcional);
                }

                seminario.OcupadoOpcional++;
                inscripto.SeminarioOpcional = seminario;
            }

            _crudSeminario.Actualizar(seminario);
            inscripto.FechaUltimaModificacion = DateTime.Now;
            _crudInscripto.Actualizar(inscripto);
        }
        
        public bool MarcarAsistenciaInscripto(ControlInscriptoDTO controlInscriptoDTO)
        {
            if (_crudAsistencia.ExisteAsistencia(controlInscriptoDTO, TipoEventoEnum.Seminario))
                return true;
            var inscripto = _crudInscripto.ObtenerPorDni(controlInscriptoDTO.Dni);
            if (inscripto == null)
                throw new ExcepcionControlada("El inscripto no existe.");
            if (controlInscriptoDTO.IdEvento == null)
                throw new ExcepcionControlada("Debe enviar un seminario para controlar.");
            if (inscripto.Seminario == null && inscripto.SeminarioOpcional == null)
                return false;
            var estaInscripto = inscripto.Seminario?.Id == controlInscriptoDTO.IdEvento ||
                                inscripto.SeminarioOpcional?.Id == controlInscriptoDTO.IdEvento;
            if (estaInscripto)
                _crudAsistencia.Insertar(new Asistencia
                {
                    Inscripto = inscripto,
                    Evento = (int) controlInscriptoDTO.IdEvento,
                    Tipo = TipoEventoEnum.Seminario
                });
            return estaInscripto;
        }
    }
}