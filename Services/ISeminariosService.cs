using System.Collections.Generic;
using Framework.Services;
using JoiiBackend.DTO;

namespace JoiiBackend.Services
{
    public interface ISeminariosService : IService
    {
        SeminarioDTO AgregarSeminario(SeminarioDTO seminarioDTO);
        IList<SeminarioDTO> ObtenerSeminarios();
        SeminarioDTO ActualizarSeminario(int id, SeminarioDTO seminarioDTO);
        void BorrarSeminario(int id);
        void Anotar(int id, bool obligatorio);
        bool MarcarAsistenciaInscripto(ControlInscriptoDTO controlInscriptoDTO);
    }
}