using System.Collections.Generic;
using Framework.Services;
using JoiiBackend.Clases.Enums;
using JoiiBackend.DTO;

namespace JoiiBackend.Services
{
    public interface IInscriptosService : IService
    {
        void CrearUsuarios();
        IList<int> ObtenerCodigos();
        void Asociar(InscriptoAsociarDTO inscriptoAsociarDTO);
        ContadoresInscriptosDTO ObtenerContadores();
        IList<InscriptoDTO> ObtenerTodos(int? filtro, TipoFiltroInscriptosEnum? tipoFiltro);
        InscriptoDTO Obtener();
        InscriptoDTO Actualizar(InscriptoDTO inscriptoDTO);
        bool ExisteInscripto(ControlInscriptoDTO controlInscriptoDTO);
        bool ExisteInscriptoFiesta(ControlInscriptoDTO controlInscriptoDTO);
        void ArmarGrupos();
    }
}