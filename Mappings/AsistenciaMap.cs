using Framework.Mapping;
using JoiiBackend.Models;

namespace JoiiBackend.Mappings
{
    public class AsistenciaMap : BaseMapConId<Asistencia>
    {
        public AsistenciaMap() : base("asistencias")
        {
            Property(x => x.Evento);
            Property(x => x.Tipo);
            ManyToOne(x => x.Inscripto);
        }
    }
}