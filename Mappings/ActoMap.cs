using Framework.Mapping;
using JoiiBackend.Models;

namespace JoiiBackend.Mappings
{
    public class ActoMap : BaseMapConId<Acto>
    {
        public ActoMap() : base("actos")
        {
            Property(x => x.Nombre);
            Property(x => x.Lugar);
            Property(x => x.FechaHora);
        }
    }
}