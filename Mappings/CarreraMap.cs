using Framework.Mapping;
using JoiiBackend.Models;

namespace JoiiBackend.Mappings
{
    public class CarreraMap : BaseMapConId<Carrera>
    {
        public CarreraMap() : base("carreras")
        {
            Property(x => x.Nombre);
        }
    }
}