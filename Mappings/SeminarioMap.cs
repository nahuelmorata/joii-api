using Framework.Mapping;
using JoiiBackend.Models;

namespace JoiiBackend.Mappings
{
    public class SeminarioMap : BaseMapConId<Seminario>
    {
        public SeminarioMap() : base("seminarios")
        {
            Property(x => x.Nombre);
            Property(x => x.Lugar);
            Property(x => x.FechaHora);
            Property(x => x.CupoObligatorio);
            Property(x => x.CupoOpcional);
            Property(x => x.OcupadoObligatorio);
            Property(x => x.OcupadoOpcional);
        }
    }
}