using Framework.Mapping;
using JoiiBackend.Models;

namespace JoiiBackend.Mappings
{
    public class VisitaMap : BaseMapConId<Visita>
    {
        public VisitaMap() : base("visitas")
        {
            Property(x => x.Nombre);
            Property(x => x.Lugar);
            Property(x => x.FechaHora);
            Property(x => x.Cupo);
            Property(x => x.Ocupado);
        }
    }
}