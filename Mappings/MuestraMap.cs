using Framework.Mapping;
using JoiiBackend.Models;

namespace JoiiBackend.Mappings
{
    public class MuestraMap : BaseMapConId<Muestra>
    {
        public MuestraMap() : base("muestras")
        {
            Property(x => x.FechaHora);
            Property(x => x.Cupo);
            Property(x => x.Ocupado);
        }
    }
}