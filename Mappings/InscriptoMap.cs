using Framework.Mapping;
using JoiiBackend.Models;

namespace JoiiBackend.Mappings
{
    public class InscriptoMap : BaseMapConId<Inscripto>
    {
        public InscriptoMap() : base("inscriptos")
        {
            Property(x => x.Nombres);
            Property(x => x.Apellidos);
            Property(x => x.Codigo);
            Property(x => x.Dni);
            Property(x => x.Lu);
            Property(x => x.Email);
            Property(x => x.Celular);
            Property(x => x.FechaNacimiento);
            Property(x => x.Fiesta);
            Property(x => x.Vegetariano);
            Property(x => x.Vegano);
            Property(x => x.Celiaco);
            Property(x => x.Certificado);
            Property(x => x.Grupo);
            Property(x => x.FechaInscripcion);
            Property(x => x.FechaUltimaModificacion);
            
            ManyToOne(x => x.Universidad);
            ManyToOne(x => x.Carrera);
            ManyToOne(x => x.Visita);
            ManyToOne(x => x.Seminario);
            ManyToOne(x => x.SeminarioOpcional);
            ManyToOne(x => x.Conferencia);
            ManyToOne(x => x.Muestra);
        }
    }
}