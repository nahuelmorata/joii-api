using Framework.Mapping;
using JoiiBackend.Models;

namespace JoiiBackend.Mappings
{
    public class UniversidadMap : BaseMapConId<Universidad>
    {
        public UniversidadMap() : base("universidades")
        {
            Property(x => x.Nombre);
        }
    }
}