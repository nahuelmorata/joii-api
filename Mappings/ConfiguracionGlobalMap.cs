using Framework.Mapping;
using JoiiBackend.Models;

namespace JoiiBackend.Mappings
{
    public class ConfiguracionGlobalMap : BaseMapConId<ConfiguracionGlobal>
    {
        public ConfiguracionGlobalMap() : base("configuracion_global")
        {
            Property(x => x.FechaFin);
            Property(x => x.FechaDescarga);
        }
    }
}