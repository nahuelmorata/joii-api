using Framework.Mapping;
using JoiiBackend.Models;

namespace JoiiBackend.Mappings
{
    public class ConferenciaMap : BaseMapConId<Conferencia>
    {
        public ConferenciaMap() : base("conferencias")
        {
            Property(x => x.Nombre);
            Property(x => x.Lugar);
            Property(x => x.FechaHora);
            Property(x => x.Cupo);
            Property(x => x.Ocupado);
        }
    }
}