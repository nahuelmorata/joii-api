CREATE TABLE IF NOT EXISTS visitas (
    Id INT(11) NOT NULL AUTO_INCREMENT,
    Nombre TEXT NOT NULL,
    Lugar TEXT NOT NULL,
    FechaHora DATETIME NOT NULL,
    Cupo INT NOT NULL,
    Ocupado INT NOT NULL DEFAULT 0,
    PRIMARY KEY(Id)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS conferencias (
    Id INT(11) NOT NULL AUTO_INCREMENT,
    Nombre TEXT NOT NULL,
    Lugar TEXT NOT NULL,
    FechaHora DATETIME NOT NULL,
    Cupo INT NOT NULL,
    Ocupado INT NOT NULL DEFAULT 0,
    PRIMARY KEY(Id)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS muestras (
    Id INT(11) NOT NULL AUTO_INCREMENT,
    FechaHora DATETIME NOT NULL,
    Cupo INT NOT NULL,
    Ocupado INT NOT NULL DEFAULT 0,
    PRIMARY KEY(Id)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS seminarios (
    Id INT(11) NOT NULL AUTO_INCREMENT,
    Nombre TEXT NOT NULL,
    Lugar TEXT NOT NULL,
    FechaHora DATETIME NOT NULL,
    CupoObligatorio INT NOT NULL,
    CupoOpcional INT NOT NULL,
    OcupadoObligatorio INT NOT NULL DEFAULT 0,
    OcupadoOpcional INT NOT NULL DEFAULT 0,
    PRIMARY KEY(Id)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS universidades (
    Id INT(11) NOT NULL AUTO_INCREMENT,
    Nombre TEXT NOT NULL,
    PRIMARY KEY(Id)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS carreras (
    Id INT(11) NOT NULL AUTO_INCREMENT,
    Nombre TEXT NOT NULL,
    PRIMARY KEY(Id)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS inscriptos (
    Id INT(11) NOT NULL AUTO_INCREMENT,
    Nombres TEXT NOT NULL,
    Apellidos TEXT NOT NULL,
    Codigo INT(9) NOT NULL,
    Dni INT NOT NULL,
    Universidad INT(11),
    Carrera INT(11),
    Lu INT NOT NULL,
    Email TEXT NOT NULL,
    Celular TEXT NOT NULL,
    FechaNacimiento DATE,
    Visita INT(11),
    Seminario INT(11),
    SeminarioOpcional INT(11),
    Conferencia INT(11),
    Muestra INT(11),
    Fiesta TINYINT(1) NOT NULL,
    Vegetariano TINYINT(1) NOT NULL,
    Vegano TINYINT(1) NOT NULL,
    Celiaco TINYINT(1) NOT NULL,
    Certificado TINYINT(1) NOT NULL,
    Grupo INT,
    FechaInscripcion DATETIME,
    FechaUltimaModificacion DATETIME,
    
    CONSTRAINT FK_inscriptos_universidades_universidad
    FOREIGN KEY (Universidad) REFERENCES universidades(Id),
    CONSTRAINT FK_inscriptos_carreras_carrera
    FOREIGN KEY (Carrera) REFERENCES carreras(Id),
    CONSTRAINT FK_inscriptos_visitas_visita
    FOREIGN KEY (Visita) REFERENCES visitas(Id),
    CONSTRAINT FK_inscriptos_seminarios_seminario
    FOREIGN KEY (Seminario) REFERENCES seminarios(Id),
    CONSTRAINT FK_inscriptos_seminarios_seminario_op
    FOREIGN KEY (SeminarioOpcional) REFERENCES seminarios(Id),
    CONSTRAINT FK_inscriptos_conferencias_conferencia
    FOREIGN KEY (Conferencia) REFERENCES conferencias(Id),
    CONSTRAINT FK_inscriptos_muestras_muestra
    FOREIGN KEY (Muestra) REFERENCES muestras(Id),
    
    PRIMARY KEY(Id),
    UNIQUE KEY(Codigo)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS configuracion_global (
    Id INT(11) NOT NULL AUTO_INCREMENT,
    FechaFin DATETIME NOT NULL DEFAULT CURRENT_DATE,
    FechaDescarga DATETIME NOT NULL DEFAULT CURRENT_DATE,
    PRIMARY KEY (Id)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS asistencias (
    Id INT(11) NOT NULL AUTO_INCREMENT,
    Inscripto INT(11) NOT NULL,
    Evento INT(11) NOT NULL,
    Tipo INT(2) NOT NULL,
    
    CONSTRAINT FK_asistencias_inscriptos_inscripto
    FOREIGN KEY (Inscripto) REFERENCES inscriptos(Id),
    
    PRIMARY KEY (Id)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS actos (
    Id INT(11) NOT NULL AUTO_INCREMENT,
    Nombre TEXT NOT NULL,
    Lugar TEXT NOT NULL,
    FechaHora DATETIME NOT NULL,

    PRIMARY KEY (Id)
) ENGINE = INNODB;