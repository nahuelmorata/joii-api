using System.Collections.Generic;
using Framework.Cruds;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds
{
    public interface ICrudInscripto : ICrudConModelId<Inscripto>
    {
        IList<Inscripto> ObtenerTodosIniciaron();
        IList<Inscripto> ObtenerPorSeminario(int idSeminario);
        IList<Inscripto> ObtenerPorVisita(int idVisita);
        IList<Inscripto> ObtenerPorUniversidad(int idUniversidad);
        IList<Inscripto> ObtenerPorFechaInscripcion(int dia);
        Inscripto? ObtenerPorCodigo(int codigo);
        bool EstaDniAsociado(int dni);
        Inscripto? ObtenerPorDni(int dni);
    }

    public interface ICrudInscriptoStateless : ICrudConModelId<Inscripto>
    {
        void CrearInscriptos(int cantidad);
    }
}