using Framework.Cruds;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds
{
    public interface ICrudActo : ICrudConModelId<Acto>
    {
        
    }
}