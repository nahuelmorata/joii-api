using Framework.Cruds;
using JoiiBackend.Clases.Enums;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds
{
    public interface ICrudAsistencia : ICrudConModelId<Asistencia>
    {
        public bool ExisteAsistencia(ControlInscriptoDTO controlInscriptoDTO, TipoEventoEnum tipoEvento);
    }
}