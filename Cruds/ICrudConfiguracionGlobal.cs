using Framework.Cruds;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds
{
    public interface ICrudConfiguracionGlobal : ICrudConModelId<ConfiguracionGlobal>
    {
        ConfiguracionGlobal ObtenerConfiguracion();
    }
}