using Framework.Cruds;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds
{
    public interface ICrudVisita : ICrudConModelId<Visita>
    {
        bool ExisteVisita(VisitaDTO visita);
    }
}