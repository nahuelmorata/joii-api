using Framework.Cruds;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds
{
    public interface ICrudCarrera : ICrudConModelId<Carrera>
    {
        
    }
}