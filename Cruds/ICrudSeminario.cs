using Framework.Cruds;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds
{
    public interface ICrudSeminario : ICrudConModelId<Seminario>
    {
        bool ExisteSeminario(SeminarioDTO seminario);
    }
}