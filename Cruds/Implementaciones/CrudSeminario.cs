using Framework.Clases.Context;
using Framework.Cruds.Implementaciones;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds.Implementaciones
{
    public class CrudSeminario : CrudConModelId<Seminario>, ICrudSeminario
    {
        public CrudSeminario(IContextSession context) : base(context)
        {
        }

        public bool ExisteSeminario(SeminarioDTO seminario)
        {
            var criteria = dbSession.QueryOver<Seminario>()
                .Where(x => x.Nombre == seminario.Nombre);
            return criteria.SingleOrDefault() != null;
        }
    }
}