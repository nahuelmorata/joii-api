using Framework.Clases.Context;
using Framework.Cruds.Implementaciones;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds.Implementaciones
{
    public class CrudConfiguracionGlobal : CrudConModelId<ConfiguracionGlobal>, ICrudConfiguracionGlobal
    {
        public CrudConfiguracionGlobal(IContextSession context) : base(context)
        {
        }

        public ConfiguracionGlobal ObtenerConfiguracion()
        {
            var criteria = dbSession.QueryOver<ConfiguracionGlobal>()
                .Where(x => x.Id == 1);
            return criteria.SingleOrDefault();
        }
    }
}