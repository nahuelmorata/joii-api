using Framework.Clases.Context;
using Framework.Cruds.Implementaciones;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds.Implementaciones
{
    public class CrudConferencia : CrudConModelId<Conferencia>, ICrudConferencia
    {
        public CrudConferencia(IContextSession context) : base(context)
        {
        }
    }
}