using Framework.Clases.Context;
using Framework.Cruds.Implementaciones;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds.Implementaciones
{
    public class CrudActo : CrudConModelId<Acto>, ICrudActo
    {
        public CrudActo(IContextSession context) : base(context)
        {
        }
    }
}