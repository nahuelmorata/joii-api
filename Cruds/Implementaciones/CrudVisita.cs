using Framework.Clases.Context;
using Framework.Cruds.Implementaciones;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds.Implementaciones
{
    public class CrudVisita : CrudConModelId<Visita>, ICrudVisita
    {
        public CrudVisita(IContextSession context) : base(context)
        {
        }

        public bool ExisteVisita(VisitaDTO visita)
        {
            var criteria = dbSession.QueryOver<Visita>()
                .Where(x => x.Nombre == visita.Nombre);
            return criteria.SingleOrDefault() != null;
        }
    }
}