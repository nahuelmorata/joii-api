using Framework.Clases.Context;
using Framework.Cruds.Implementaciones;
using JoiiBackend.Clases.Enums;
using JoiiBackend.DTO;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds.Implementaciones
{
    public class CrudAsistencia : CrudConModelId<Asistencia>, ICrudAsistencia
    {
        public CrudAsistencia(IContextSession context) : base(context)
        {
        }

        public bool ExisteAsistencia(ControlInscriptoDTO controlInscriptoDTO, TipoEventoEnum tipoEvento)
        {
            Inscripto? inscriptosAlias = null;
            var criteria = dbSession.QueryOver<Asistencia>()
                .JoinAlias(x => x.Inscripto, () => inscriptosAlias)
                .Where(x => inscriptosAlias!.Dni == controlInscriptoDTO.Dni)
                .And(x => x.Evento == controlInscriptoDTO.IdEvento)
                .And(x => x.Tipo == tipoEvento);
            return criteria.SingleOrDefault() != null;
        }
    }
}