using Framework.Clases.Context;
using Framework.Cruds.Implementaciones;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds.Implementaciones
{
    public class CrudUniversidad : CrudConModelId<Universidad>, ICrudUniversidad
    {
        public CrudUniversidad(IContextSession context) : base(context)
        {
        }
    }
}