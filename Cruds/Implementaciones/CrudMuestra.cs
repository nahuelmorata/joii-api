using Framework.Clases.Context;
using Framework.Cruds.Implementaciones;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds.Implementaciones
{
    public class CrudMuestra : CrudConModelId<Muestra>, ICrudMuestra
    {
        public CrudMuestra(IContextSession context) : base(context)
        {
        }
    }
}