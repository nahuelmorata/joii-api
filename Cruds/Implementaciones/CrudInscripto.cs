using System;
using System.Collections.Generic;
using Framework.Clases.Context;
using Framework.Cruds.Implementaciones;
using JoiiBackend.Models;
using NHibernate;

namespace JoiiBackend.Cruds.Implementaciones
{
    public class CrudInscripto : CrudConModelId<Inscripto>, ICrudInscripto
    {
        public CrudInscripto(IContextSession context) : base(context)
        {
        }

        public IList<Inscripto> ObtenerTodosIniciaron()
        {
            var criteria = dbSession.QueryOver<Inscripto>()
                .Where(x => x.FechaUltimaModificacion != null);
            return criteria.List();
        }
        
        public IList<Inscripto> ObtenerPorSeminario(int idSeminario)
        {
            Seminario? seminarioAlias = null;
            var criteria = dbSession.QueryOver<Inscripto>()
                .JoinAlias(x => x.Seminario, () => seminarioAlias)
                .Where(x => seminarioAlias!.Id == idSeminario);
            return criteria.List();
        }
        
        public IList<Inscripto> ObtenerPorVisita(int idVisita)
        {
            Visita? visitaAlias = null;
            var criteria = dbSession.QueryOver<Inscripto>()
                .JoinAlias(x => x.Visita, () => visitaAlias)
                .Where(x => visitaAlias!.Id == idVisita);
            return criteria.List();
        }
        
        public IList<Inscripto> ObtenerPorUniversidad(int idUniversidad)
        {
            Universidad? universidadAlias = null;
            var criteria = dbSession.QueryOver<Inscripto>()
                .JoinAlias(x => x.Universidad, () => universidadAlias)
                .Where(x => universidadAlias!.Id == idUniversidad);
            return criteria.List();
        }
        
        public IList<Inscripto> ObtenerPorFechaInscripcion(int dia)
        {
            var criteria = dbSession.QueryOver<Inscripto>()
                .Where(x => x.FechaInscripcion != null)
                .And(x => ((DateTime) x.FechaInscripcion!).Day == dia);
            return criteria.List();
        }

        public Inscripto? ObtenerPorCodigo(int codigo)
        {
            var criteria = dbSession.QueryOver<Inscripto>()
                .Where(x => x.Codigo == codigo);
            return criteria.SingleOrDefault();
        }

        public bool EstaDniAsociado(int dni)
        {
            var criteria = dbSession.QueryOver<Inscripto>()
                .Where(x => x.Dni == dni);
            return criteria.SingleOrDefault() != null;
        }

        public Inscripto? ObtenerPorDni(int dni)
        {
            var criteria = dbSession.QueryOver<Inscripto>()
                .Where(x => x.Dni == dni);
            return criteria.SingleOrDefault();
        }
    }

    public class CrudInscriptoStateless : CrudStatelessConModelId<Inscripto>, ICrudInscriptoStateless
    {
        public CrudInscriptoStateless(IContextSession session) : base(session)
        {
        }

        public void CrearInscriptos(int cantidad)
        {
            for (var i = 0; i < cantidad; i++)
            {
                var inscripto = new Inscripto
                {
                    Codigo = ObtenerCodigo()
                };
                dbSession.Insert(inscripto);
            }
        }

        private bool ExisteCodigo(int codigo)
        {
            var criteria = dbSession.QueryOver<Inscripto>()
                .Where(x => x.Codigo == codigo);
            return criteria.SingleOrDefault() != null;
        }

        private int ObtenerCodigo()
        {
            var random = new Random();
            var codigo = random.Next(100000000, 999999999);
            while (ExisteCodigo(codigo))
                codigo = random.Next(100000000, 999999999);
            return codigo;
        }
    }
}