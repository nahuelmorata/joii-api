using Framework.Clases.Context;
using Framework.Cruds.Implementaciones;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds.Implementaciones
{
    public class CrudCarrera : CrudConModelId<Carrera>, ICrudCarrera
    {
        public CrudCarrera(IContextSession context) : base(context)
        {
        }
    }
}