using Framework.Cruds;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds
{
    public interface ICrudConferencia : ICrudConModelId<Conferencia>
    {
        
    }
}