using Framework.Cruds;
using JoiiBackend.Models;

namespace JoiiBackend.Cruds
{
    public interface ICrudMuestra : ICrudConModelId<Muestra>
    {
        
    }
}