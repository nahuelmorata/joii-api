using Framework.Clases.DB;
using Framework.Clases.NHibernate;
using JoiiBackend.Models;

namespace JoiiBackend.Clases.NHibernate
{
    public class JoiiNHSessionFactorySimple : NHSessionFactorySimpleCustom<Inscripto>
    {
        public JoiiNHSessionFactorySimple(DBConfiguracion dbConfiguracion) : base(dbConfiguracion)
        {

        }
    }
}
