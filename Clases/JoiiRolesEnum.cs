using Framework.Models;

namespace JoiiBackend.Clases
{
    public class JoiiRolesEnum : RolesEnum
    {
        public static JoiiRolesEnum Seminario = new JoiiRolesEnum(2, nameof(Seminario));
        public static JoiiRolesEnum Visita = new JoiiRolesEnum(3, nameof(Visita));
        public static JoiiRolesEnum Inscripcion = new JoiiRolesEnum(4, nameof(Inscripcion));
        public static JoiiRolesEnum Inscripto = new JoiiRolesEnum(5, nameof(Inscripto));
        public static JoiiRolesEnum Control = new JoiiRolesEnum(6, nameof(Control));
        
        private JoiiRolesEnum(int id, string name)
            : base(id, name)
        {
        }
    }
}