namespace JoiiBackend.Clases.Enums
{
    public enum TipoFiltroInscriptosEnum
    {
        Seminario, Visita, Universidad, FechaInscripcion
    }
}