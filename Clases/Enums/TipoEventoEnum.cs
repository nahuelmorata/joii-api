namespace JoiiBackend.Clases.Enums
{
    public enum TipoEventoEnum
    {
        Conferencia, Muestra, Seminario, Visita, Acto
    }
}