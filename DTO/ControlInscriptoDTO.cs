using Framework.DTO;

namespace JoiiBackend.DTO
{
    public class ControlInscriptoDTO : IDTO
    {
        public virtual int Dni { get; set; }
        public virtual int? IdEvento { get; set; }
    }
}