using Framework.DTO;

namespace JoiiBackend.DTO
{
    public class ContadoresInscriptosDTO : IDTO
    {
        public virtual int Global { get; set; } = 0;
        public virtual int Dia { get; set; } = 0;
    }
}