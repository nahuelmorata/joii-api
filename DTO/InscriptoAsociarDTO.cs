using Framework.Clases;
using Framework.DTO;

namespace JoiiBackend.DTO
{
    public class InscriptoAsociarDTO : IDTO
    {
        public virtual int Dni { get; set; } = 0;
        public virtual int Codigo { get; set; } = 0;

        public override void Validar()
        {
            this.Controlar(() => Dni.ToString().Length is >= 7 and <= 9, "Ingrese un dni valido.");
            this.Controlar(() => Codigo != 0, "Ingrese un codigo valido.");
        }
    }
}