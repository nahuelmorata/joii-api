using System;
using Framework.DTO;

namespace JoiiBackend.DTO
{
    public class FechaDTO : IDTO
    {
        public virtual DateTime Fecha { get; set; }
    }
}