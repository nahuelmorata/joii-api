using Framework.DTO;

namespace JoiiBackend.DTO
{
    public class UniversidadDTO : DTOConID
    {
        public virtual string Nombre { get; set; } = "";
    }
}