using System;
using Framework.DTO;

namespace JoiiBackend.DTO
{
    public class MuestraDTO : DTOConID
    {
        public virtual DateTime FechaHora { get; set; } = DateTime.Now;
        public virtual int Cupo { get; set; } = 0;
        public virtual int Ocupado { get; set; } = 0;
    }
}