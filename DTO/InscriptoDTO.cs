using System;
using Framework.DTO;

namespace JoiiBackend.DTO
{
    public class InscriptoDTO : DTOConID
    {
        public virtual string Nombres { get; set; } = "";
        public virtual string Apellidos { get; set; } = "";
        public virtual int Codigo { get; set; }
        public virtual int Dni { get; set; }
        public virtual UniversidadDTO? Universidad { get; set; }
        public virtual int Lu { get; set; }
        public virtual CarreraDTO? Carrera { get; set; }
        public virtual string Email { get; set; } = "";
        public virtual string Celular { get; set; } = "";
        public virtual DateTime? FechaNacimiento { get; set; }
        public virtual VisitaDTO? Visita { get; set; }
        public virtual SeminarioDTO? Seminario { get; set; }
        public virtual SeminarioDTO? SeminarioOpcional { get; set; }
        public virtual ConferenciaDTO? Conferencia { get; set; }
        public virtual MuestraDTO? Muestra { get; set; }
        public virtual bool Fiesta { get; set; }
        public virtual bool Vegetariano { get; set; }
        public virtual bool Vegano { get; set; }
        public virtual bool Celiaco { get; set; }
        public virtual bool Certificado { get; set; }
        public virtual int? Grupo { get; set; }
        public virtual DateTime? FechaInscripcion { get; set; }
        public virtual DateTime? FechaUltimaModificacion { get; set; }
    }
}