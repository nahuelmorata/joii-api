using System;
using Framework.DTO;

namespace JoiiBackend.DTO
{
    public class ActoDTO : DTOConID
    {
        public virtual string Nombre { get; set; } = "";
        public virtual string Lugar { get; set; } = "";
        public virtual DateTime FechaHora { get; set; }
    }
}